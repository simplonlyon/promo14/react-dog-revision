import axios from "axios";


export class DogService {

    static async fetchAll() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/dog');
        return response.data;
    }

    static async search(term) {

        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/dog?search='+term);
        return response.data;
    }

    static async add(dog) {

        const formData = new FormData();
        formData.append('name', dog.name);
        formData.append('breed', dog.breed);
        formData.append('picture', dog.picture);

        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/dog', formData);
        return response.data;
    }
}