

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { Grid, makeStyles } from '@material-ui/core';
import { green, red, yellow } from '@material-ui/core/colors';
import { useState } from 'react';

const useStyles = makeStyles({
    notAvailable: {
        color: red[500]
    },
    available: {
        color: green[500]
    },
    selected: {
        backgroundColor: yellow[200],
        borderColor: yellow[500]
    }
});

export function SelectableList({ dogs }) {
    const classes = useStyles();

    const [selected,setSelected] = useState([]);

    function toggleSelect(dog) {
        if(selected.includes(dog)) {
            setSelected(
                selected.filter(item => item.id !== dog.id )
            );
        } else {
            setSelected([
                ...selected,
                dog
            ]);
        }
    }
    

    return (
        <div>
            <Grid container spacing={3}>
                {dogs.map(dog => (
                    <Grid key={dog.id} item xs={12} sm={4}>
                        <Card raised={selected.includes(dog)} className={selected.includes(dog) ? classes.selected:''} onClick={() => toggleSelect(dog)} variant="elevation">
                            <CardActionArea>
                                <CardMedia
                                    component="img"
                                    image={dog.picture.startsWith('http')?dog.picture:process.env.REACT_APP_SERVER_URL+dog.picture}
                                    title={dog.name}
                                    alt={dog.name}
                                />
                                <CardContent>
                                    <Typography gutterBottom component="h2" >
                                        {dog.name}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        Breed: {dog.breed}
                                    </Typography>
                                    <Typography align="right" className={dog.available ? classes.available : classes.notAvailable} component="p">
                                        {dog.available ? "Available" : "Not Available"}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>

                        </Card>
                    </Grid>
                ))}
            </Grid>
        </div>
    );
}