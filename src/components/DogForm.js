import { useState } from "react";


const initial = {
    name: '',
    breed:'',
    picture:''
}
export function DogForm({onSubmit}) {
    const [form,setForm] = useState(initial);

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]:event.target.value
        });
    }

    const handleFile = (event) => {
        setForm({
            ...form,
            [event.target.name]:event.target.files[0]
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit(form);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input placeholder="name" type="text" name="name" onChange={handleChange} value={form.name}  />
            <input placeholder="breed" type="text" name="breed" onChange={handleChange} value={form.breed}  />
            <input placeholder="picture" type="file" name="picture" onChange={handleFile} />
            <button>Submit</button>
        </form>
    );
}