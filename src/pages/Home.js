import { useEffect } from "react";
import { useState } from "react";
import { DogForm } from "../components/DogForm";
import { SearchBar } from "../components/SearchBar";
import { SelectableList } from "../components/SelectableList";
import { DogService } from "../services/DogService";




export function Home() {

    const [dogs, setDogs] = useState([]);

    useEffect(() => {
        async function fetchDogs() {
            const data = await DogService.fetchAll();
            setDogs(data);
        }
        fetchDogs();
    }, []);

    const handleSearch = async (search) => {
        const data = await DogService.search(search);
        setDogs(data);
    }

    const handleAdd = async (dog) => {
        const added = await DogService.add(dog);
        setDogs([
            ...dogs,
            added
        ]);
    }
    
    return (
        <div>
            <SearchBar onSearch={handleSearch} />
            <DogForm onSubmit={handleAdd}/>
            <SelectableList dogs={dogs} />
        </div>
    );
}